#!/bin/bash

if [ "$DEBUG" == "TEST" ]; then
    set -x
fi

cd $(dirname "$0")

docker image build -t software-blog:latest .
