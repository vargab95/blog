FROM node:18-alpine as builder

WORKDIR /app

COPY ./package.json ./package-lock.json ./
RUN npm install

COPY ./ ./
RUN ./node_modules/hexo/bin/hexo generate


FROM nginx:1.23-alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/public /var/www
