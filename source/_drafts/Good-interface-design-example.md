title: An example for excellent interface design
  - Node.js
  - Software design
categories:
  - Software design
date: 2023-10-23 21:34:00
---


https://www.npmjs.com/package/bcrypt

designed for safe usage.

I listened to a SEC conference talk on youtube when realized that I forgot to
include salt from my authentication component. I created the ticket to fix it,
however when I opened the code I saw the genSalt function. I was astonished
because I remembered that I forgot to include this. It turned out that the
interface design of bcrypt has kind of enforced using salt. It has a required
salt parameter, so I had to use the library securely. In my opinion this is the
perfect interface design which enforces best practices. Nowadays several
interfaces are using optionals to make an interface usable in several
circumstances. However, this case proved to me that it's better to enforce
parameters which you know as library developer would most likely be used by all
users.
