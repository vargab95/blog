title: Converting a C app to Rust
author: John Doe
tags: []
categories: []
date: 2023-01-12 22:07:00
---

Cargo is a pain. A crates.io index update took at least 5 minutes when adding a
new package... Maybe my network is wrong.

It's great to have separate packages for everything. chrono, regex, etc.
Nothing is built in. You need to specify the packages what you need and you
won't get a bloated language. I really love it.

Drives your way. In object oriented world a lot of developers are creating
classes for literally everything. I fell into the same trap in this codebase.
I've tried to create and empty History struct which implements functions like
list, add, clean_up, etc. Then I've tried to use it. That was the point when
I've realised that it's completely unnecessary. These are just a bunch of pure
functions. So I could refactor it immediately.

It makes you think about all error cases. In OO languages and even in C it's
easy to mitigate some problems. Like a function which connects to your
database. Will it fail? Rarely. So how do you call it in f.e. nodejs? await
db.connect. But what will happen if the the database is restarting? It will
fail, throw an exception and result in an unhadled exception or rejection. What
happens in rust? You will have to explicitely match the error case (if not
using unwrap). I think it's great because in my experience, if a developer says
that this error could not occur, it will sooner or later.

It makes you think about optional entities (if you don't use unwrap). For
example, in C, you usually just pass a pointer. However, it's easy (I did it
several times) to assume in a lower level code that it'll be specified, of
course. This parameter must be filled every time. Then a new dev comes and
calls it with a NULL, resulting in a crash. It's not fun when a database server
consuming 100 GB of RAM crashes and you're the one who has to find the root
cause...

I couldn't bring myself to do it. It tells something to me about rust that
someone who likes to try out new technologies does not want to code in it.

Summary
- You must not use unwrap!
