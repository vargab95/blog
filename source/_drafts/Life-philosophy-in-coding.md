title: Life philosophy in coding
author: Balázs János Varga
tags:
  - Workflow
categories:
  - Workflow
date: 2023-06-06 12:33:00
---



Applying life philosophy techniques in code. Like goal internalization.
