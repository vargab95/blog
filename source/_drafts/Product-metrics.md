title: Product metrics
author: Balázs János Varga
tags:
  - Workflow
categories:
  - Workflow
date: 2023-05-09 08:28:00
---

Listen to Understanding Software by Max Kanat-Alexander on Audible. https://www.audible.co.uk/pd/1469074354?source_code=ASSORAP0511160007

Chapter 19. Measuring eveloper productivity

I was thinking while I listened to this chapter about productivity measurement. Idea: to measure productivity and product quality with
- number of bugs detected over last month or year
- feature time from idea to delivery
- bug resolution time

I will try this approach.

Write the definition of bug and feature.
