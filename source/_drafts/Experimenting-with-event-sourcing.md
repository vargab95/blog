title: Experimenting with event sourcing
author: John Doe
date: 2023-01-07 09:05:50
tags:
---
# The problem domain

Finance manager project
I have not found any appropriate tool which is customizable enough.
Decided to record all financial events in an event store.

# First steps

Reading Martin Fowlers document https://martinfowler.com/eaaDev/EventSourcing.html
Watching youtube videos

# Problems

That was great to read about all the abstract stuff, like replayability,
scalability and the capability of auditing. However, I had a lot of questions
how to build such a system. Usually, youtube videos and articles provides
either a highly abstract view of the architectural pattern or just gives a
short example of it, using a few endpoints and a single database. It's great
until you don't want to build one on your own. At this point I was lost in my
thoughts so I decided to write a blog article where I describe how do I get
from a known but sofar unused architectural pattern to the design and
implementation of a project based on that pattern.

## How to authenticate?


## How to handle failed requests?

Like if the user tries to add a category which already exists. Should it be
stored in the event log? If yes, then why and how to sign that it had failed.
If not, then which component should reject it?

## How to ensure consistency?

## How to make sure each event will be handled?

Even if the consumer crashes during request handling.

"An example of this would be a system with lots of readers and a few writers.
Using Event Sourcing this could be delivered as a cluster of systems with
in-memory databases, kept up to date with each other through a stream of
events. If updates are needed, they can be routed to a single master system (or
a tighter cluster of servers around a single database or message queue) which
applies the updates to the system of record and then broadcasts the resulting
events to the wider cluster of readers."

# First steps of the design

Authentication with a separate service which only handles auth. This will be
called with every request and it'll update the request headers with the
authenticated user id. I know, OAuth can be used and then authorization is also
possible, but let's do it step by step. So at first, a simple authentication
will be designed and implemented and if it's necessary, a more complex auth
flow will follow.

Handling financial update events should also be a separate service. It should
serve as the event producer for external requests. The problem with this
approach is that this service must also have a read view of the events, as it
would be required to be able to reject invalid requests, like adding the same
bucket for the same user twice.

As the

# Extending the functionality

As the available material on this pattern states that it's easily extensible,
the next step to experiment with it is to add a new functionality. This new
functionality would be the bucket notification system. This new component would
retrieve the bucket update events and if a limit is defined for one and that
limit is exceeded it would generate a new, BucketLimitExceeded event.

The next step is to consume this event. For that, I would use a new component,
called Mailer. It would send an email for a user who has specified it's email
using a template for the BucketLimitExceeded event. Beside that configuration
functionality, the mailer would also serve as a gateway to the SMTP server. It
gonna store all the sent email in an event log like structure to be able to
determine whether that mail was already sent out or not.
