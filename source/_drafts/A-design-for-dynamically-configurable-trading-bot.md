title: A design for dynamically configurable trading bot
author: Balázs János Varga
tags:
  - crypto
  - trading
  - python
  - software design
categories:
  - software design
date: 2022-02-22 22:28:00
---

Introduction

Previous design
It was not bad, but several things couldn't be done

Components
	fetchers
    filters
    detectors
    traders

Components' responsibilities
Why do we need them

Current design

What is it good for
Observer pattern
Multiple signal buses
Components
	fetchers
    filters
    detectors
    detector combinations
    traders

Components' responsibilities
Why do we need them


```plantuml
@startuml
interface Fetcher {
	+fetch_technical_indicator()
    +get_technical_indicator()
    +get_indicator_history()
}
@enduml
```

Link to trader-bot
It's an example, however does not completely implements the design
