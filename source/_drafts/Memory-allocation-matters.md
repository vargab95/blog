title: Memory allocation matters
author: Balázs János Varga
tags:
  - C
  - Software design
categories:
  - Software design
date: 2023-09-11 18:35:00
---

# Introduction

In higher level languages people tend to forgot about the fact that there is a
hardware in the end which will execute their code. The variables and objects
what they create must be allocated in one of the memory chips in the computer
which runs the software and as nothing, it also does not come for free. There
is a time cost for writing and reading to this device which is most likely an
external component to the CPU. Nowadays, most of the hardware aspects are
hidden behind an abstraction and developers tend to not care about the certain
quirks of the underlying hardware. Even amongst developers whom using lower
level languages, there are some common misconceptions.

# Using the "right" data structure

I've been taught that if there is a set of data which is frequently modified
and in most of the cases accessed by iterating through it rather then with a
random access, then a linked list should be used in C. It can be modified by
just manipulating pointers so it must be faster than moving large pieces of
data, like reallocating an array. Of course, it can be true, but I was
astonished when I measured it. Up to a certain count of items, reallocating the
whole array to extend it is much faster then allocating some new memory and
manipulating pointers.

# Affect of a different allocator

Even if the most performant data structure is used for a certain purpose, the
memory allocation can still have a huge effect. As you may know, CPUs have
multiple levels of caches. As with any cache, there is a cost of maintaining a
cache. If cache hits are not frequent enough, then a cache can even slow down
your process. It's similar in CPUs. If you have an array, it allocates by
definition a consecutive area of memory. However, lists are just allocation
small chunks of a few (or maybe a few hundred) bytes and points to another
chunk. This way, when you have a list of items, you may read up a different
page for each list item. This is hugely inefficient.

There are solutions for this. For example different allocators, like arena
allocators. Nowadays, C standard libraries, like glibc already has some built
in optimizations in malloc, like
[arenas](https://sourceware.org/glibc/wiki/MallocInternals#Arenas_and_Heaps).
It can give some additional performance, as kernel function should only be
called when a new arena is needed, so the user space - kernel space boundary
may not be crossed at every allocation. However, in certain cases cannot know
your allocation pattern. So there is an optimization possibility here.
Developers can implement their own allocators above the malloc interface to
handle certain allocation patterns in the most performant ways.

Fixed allocator for cases where the size is known in advance. Arena allocator
for unknown sized, but consecutive allocations which have a short lifetime and
slab allocators for allocation groups with long lifetimes.

Time taken by using different allocator algorithms. It can be a lot. Examples
using m_mem and m_map. https://ftp.tu-chemnitz.de/pub/Local/urz/ding/de-en/

Arena & fixed allocator are 30% faster on my machine than glibc's malloc
implementation. I know it's just a micro benchmark, but I think it clearly
shows that using the correct allocation pattern can result in much better
performance.

# Conclusion

As I shown it to you, choosing a correct data structure can be more complex
then it seems for the first time. It depends on the access pattern and on the
amount of data. My favorite approach is always to prototype and measure before
production use. Furthermore, even if the correct data structure is chosen, to
achieve the best possible performance, you must consider using the appropriate
allocator as well.
