---
title: Projects
no_date: true
no_comments: true
no_about: true
no_toc: true
---

<div class="card-grid">
    <div class="card">
        <!-- <div class="cover-img">
            <img src="/img/dbhistory_logo.png" alt="The logo of dbhistory">
        </div> -->
        <div class="content">
            <p class="title">DBHistory</p>
            <p class="description">A command line tool managing directory based history.</p>
        </div>
        <div class="actions">
            <div class="left">
                <span class="badge no-select">Stable</span>
            </div>
            <div class="right">
                <a class="action-button-primary" href="https://gitlab.com/vargab95/dbhistory">Learn more</a>
            </div>
        </div>
    </div>
    <div class="card">
        <!-- <div class="cover-img">
        <div class="cover-img">
            <img src="/img/000025.png" alt="The logo of the trader bot">
        </div> -->
        <div class="content">
            <p class="title">Trader bot</p>
            <p class="description">Bot for trading with crypto currencies.</p>
        </div>
        <div class="actions">
            <div class="left">
                <span class="badge warning no-select">Beta</span>
            </div>
            <div class="right">
                <a class="action-button-primary" href="https://gitlab.com/vargab95/trader-bot">Learn more</a>
            </div>
        </div>
    </div>
</div>
---

<p class="text-center"><a href="https://gitlab.com/users/vargab95/projects">View all on GitLab</a></p>

---
