---
title: Resume
layout: page
no_date: true
no_comments: true
no_about: true
no_toc: true
---

---

# Short introduction

I'm a software engineer with a several years of experience who currently works
as a senior software developer. Previously, I developed embedded applications,
computer vision algorithms, websites, REST APIs, crypto currency trading bots,
worked on telecommunication nodes and I also worked in the ops field using
docker swarm. Coding and software design is my passion and hobby so I regularly
do home projects to deepen my knowledge of professional fields of interest.

# Work experience

I'm working as a software developer since 2015. In my full time jobs, I have
mostly worked with C, C++ and Python. These are the languages what I have the
most experience with so I would say they are my biggest strength currently. For
details of my full time jobs, please checkout my linked in profile. You can
found the link at the end of this page.

As I wanted to learn more and more about software development and design, I've
started to work as a freelancer in the evenings and on the weekends as well. It
gave me a great opportunity to participate in small to mid sized green field
projects and learn new technologies, to learn best practices, to design and
build software starting from some ideas. In these projects I've worked
extensively with docker, I was teaching C, C++ and Python for a several months,
I had the chance to work in an ops team rather then a development one for a
while, I worked with Python and NodeJS to implement REST APIs and systems
consisting of several services and the list goes on. Working with so much
different technologies and in different areas gave me the chance to have
experience from different areas of software development and it extensively
broadens my knowledge in all different topics of creating and maintaining
software.

# Formal education

I have learned at the University of Óbuda, Budapest, Hungary from 2013 until
2017 . I got a bachelor's degree there in electrical and electronics
engineering.


<div class="card-grid" style="margin-top: 10vh">
    <div class="card">
        <div class="content">
            <p class="title">Projects</p>
            <p class="description">View my public projects.</p>
        </div>
        <div class="actions">
            <div class="right">
                <a class="action-button-primary" href="/projects/">View</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="content">
            <p class="title">GitLab</p>
            <p class="description">My GitLab profile.</p>
        </div>
        <div class="actions">
            <div class="right">
                <a class="action-button-primary" href="https://gitlab.com/vargab95">View</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="content">
            <p class="title">LinkedIn</p>
            <p class="description">My LinkedIn profile.</p>
        </div>
        <div class="actions">
            <div class="right">
                <a class="action-button-primary" href="https://www.linkedin.com/in/bal%C3%A1zs-varga-38b075129/">View</a>
            </div>
        </div>
    </div>
</div>

