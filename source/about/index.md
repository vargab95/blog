---
title: About
layout: page
no_date: true
no_comments: true
no_about: true
no_toc: true
---

# What is this site about?

This site was created to serve as kind of diary of my work as a software
developer. It gives me a chance to have records of my earlier thoughts in
software development topics like design, my home projects and way of working in
my professional experience. It's great because with these records I can later
recall the reason for some of my previous decisions and my previous thoughts on
different topics, like how to design a software, how to find a bug, what is the
best way of working. As I like to retrospectively revise my professional
development, it would serve as a good source beside the git repositories.
Furthermore, it gives a chance to my future employers to see into my developer
mindset.

# What are my plans?

As I've already described in the previous chapter, I'm planning to write a kind
of diary of the software development activities in my life. As I have several
years of experience when I start to write this blog, I'll try to remember to
the intentions and decisions of my previous projects and write some posts about
them and I'll write new posts as well as I face new problems or interesting
topics. At the end, I would like to record some information about most of my
home projects, interesting problems what I've faced in my career, the change in
my mindset about processes, my developer and every day workflows, my opinion
about different approaches and trends and so on.

# What you won't find here?

I'm a bit fed up by finding outdated, irrelevant, basic posts when I try to
search for details on a certain topic or some analysis already made by others
to help me decide how to implement a certain feature. For example, when I
started to work on a nodejs project I searched for best practices on
authentication with express. The first page of google was full of simple
tutorials, how to use JWT with a custom middleware and some ORM and of course
some basic code samples. However, there was no analysis which considered
different solutions, trade offs between them, available modules to use and so
on. I think, it does not bring value to create the next quick start guide,
because there are loads of them already on the internet. I value the mentioned
analysis articles more. So in this blog, you should never find any quick start
guide or basic code snippet for a common problem which was described by
hundreds or thousands just to create a new post. You would rather find
theoratical discussions and thought processes about choosing a specific
technology, solution, process, etc or my opinion about certain fields of
software development.

# How changes are handled?

I think, it's important to avoid changing already released blog posts as much
as possible, but sometimes it cannot be avoided. To make sure, that everyone
can find the exact version of my posts which he or she have read, I include the
commit hash of the actually deployed version in the footer and make the project
open source. So if anyone wants to look up an older version of a blog post
which have changed, they'll be able to checkout the commit hash if they've
saved it or just to find the proper one in the commit history, then check that
version out and run on their local machine or read the markdown source using
gitlab. The project can be found [here](https://gitlab.com/vargab95/blog).
