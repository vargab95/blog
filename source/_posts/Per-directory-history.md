title: Per directory history
author: Balázs János Varga
tags:
  - C
  - Command line
  - Linux
categories:
  - Software design
  - Workflow
date: 2023-03-24 10:04:00
---

In this post, I would like to describe one of my projects what I've started to
implement years ago. In this post, I would like to describe only the basic idea
and how have I started the implementation and why. Maybe further posts will
follow about the modifications implemented since then.

# The problem

As a developer who uses linux and command line tools in his daily workflow, I
used bash history a lot. However, I usually work on several projects in
parallel. Or at least on several tasks of the same project. For example, I
usually have several open tickets in parallel. For each, I do some grepping, I
run some commands to try to reproduce them or to test a solution. However, when
I have such a task and for example I continue to work on it after let's say a
week. Then it's a pain to remember what have I done or how. Which commands have
I already run on which files, etc. To filter out from bash history which
command I ran on that project is impossible. Furthermore, when I used several
terminals in parallel, bash history usually got messed up.

# First approach

I thought about it how could I make my working hours more effective. I've
realized that I would need some kind of simple description what have I done,
how and which commands I've used for the tests. Ok, so I need a file I tought.
However, I've continously forgot to add the information to the description. For
example when I was interrupted by an incoming call or when I was simply too
deep in an investigation. After a few days it was clear for me, it won't be the
solution. But then how can I have a simple, automatic, per project or per task
approach to record the work history.

# Second approach

I started analyze the problem in more depths. The first question was, what is a
project or a task for me? I usually use the following directory structure in my
home folder.

I have a workspace folder which stores the projects which I work on. There is a
subfolder for each project. All project folders contain at least two
subfolders. One is for the source code. In case of multi-repo git project it
contains multiple git repositories and in case of subversion projects it
contains multiple branches of the same project as branching is not as
convenient as in git using subversion. The another one is the tasks subfolder.
If contains a subfolder for each task for which I have to store some files,
like logs, specification, etc.

- workspace
  - project 1
    - sources
      - repository / repositories
    - tasks
      - feature 1
      - feature 2
      - bug 1
      - bug 2
  - project 2
    - ...

Based on this structure, it was clear that I could bind the history of a
project or task based on directories. Firstly, I've searched for a bash history
feature which can do it for me. As I did not found any and there were some
other problems with bash history as well, I've decided to implement it on my
own.

# Requirements

## Functional requirements

So the first question was what are the requirements for this tool. Which use
cases should it handle. The first and most important is to be able to store the
bash history per directory. For example, I can have different compile and test
commands in different projects. So it would be great to be able to show the
commands executed in the current directory.

Another use case can be when I work on a project's user story or bug, then I
would like to know all the commands executed before in this folder and in the
subfolders. These can be the commands for all the greps executed on the log
files and all commands which are used to reproduce the problem.

The last use case what I wanted a tool for is to search the whole history,
because sometimes I had some useful commands which I knew that I've executed
but I had no idea in which project. Of course, one can use bash history for
this purpose but it did not work as expected when I've used multiple terminals
or SSH sessions. In this case it sometimes missed some of the commands which is
not acceptable for me.

To summarize it, the tool must support at least the following features:
- List the bash commands executed in the current working directory
- List the bash commands executed in another directory
- List the bash commands executed in a directory and it's subdirectories
- Search the bash commands by path

## Additional requirements

Beside the functional ones, there are some additional requirements. For
example, I'll use this tool in several environments, like remote servers,
laptops, virtual machines, etc. So it should be easily portable and
installable.


# Design

## Storage

I like the KISS approach, especially in simple tools like this. So I started to
think about it from the simplest possible approach. To store a file in each
directory and collect the list of commands there. Later these files can be
found and search for by simple bash aliases. At first glance, it seems to be a
simple and usable approach. So I've tested it with a few folders. It was a
mess. I've accidentaly deleted some files and I had to modify a lot of
.gitignore files just to not commit bash history to the project repository. So
it was clear that the history should be stored in a central place where I can
use a simple copy to back it up and it does not mess up my repositories.

The next idea was to create a dotfile in my home directory and store all the
history there. Similarily as bash history does. However, during these initial
tests it was clear that additional requirements may be added later. Like
ability to clean up the history, because in a few days I've added a few
thousand entries to my new history. Furthermore, it would be nice in the future
to detect and handle renaming of folders.

As I had some further ideas to improve the tool and the file based approach did
not fit perfectly with my plans, I've started to think on a more complex
storage method. As it should be extensible and I did not want to duplicate the
path over and over again I thought that a RDBMS would be a perfect fit for my
needs. It is easily extensible, I can simply join tables to not store the path
multiple times and if I'll need some more queries in the future I could simply
add some new SQL commands.

### Database schema

Now it's decided that an RDBMS will be used. So my next step was to create a
database schema.

<div class="plantuml-container">
```plantuml
@startuml
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "path_map" as path_map {
  *id: number <<generated>>
  --
  *path: text
}

entity "history" as history {
  *id: number <<generated>>
  --
  *path_id: number <<FK>>
  *command: text
  *timestamp: datetime
}

path_map ||..o{ history
@enduml
```
</div>

As you can see, it is a really simple database schema. It provides a table for
storing paths and another one for storing the history entries. Using this
simple schema, paths are stored only once, it's extensible and I can search in
paths and history entries with simple SQL commands.

### Selecting the database engine

As a next step I've selected the database engine. To be honest it was not a
real selection. I needed a well maintained SQL database which supports simple,
local, file based storage and works well on linux. I think nowadays most of the
developers would immediately say SQLite. So I chose the "industry standard".

## Configuration

As it'll be a simple tool for a simple purpose, it won't have so much
configuration options, but I was quite sure that I'll need a few. For example,
to be able to set a log level to be able to easily find problems. To be able to
set the parameters for the cleanup mechanism, etc. For this feature, I went
with the dotfile approach usually used in linux distributions. The tool will
parse the $HOME/.dbhistory.ini configuration file by default.

I chose the following options to be configurable:
- database_path
- log_file_path
- log_level
- deletion_time_threshold
- max_command_length

Database path, log file path and log level are quite straightforward. I've
introduced them because in case of some tools, I had some issues by their
default values. For example, some linux command line tools had a default path
which I did not usually use for storing my configuration files. Of course,
links can be created, etc. However I did not like the fact that I could not
redefine it and store them where I store the rest of my configuration files. So
I've decided to add configuration option for the path of every file which this
tool creates.

### deletion_time_threshold

As it was described earlier, I wanted to have a cleanup mechanism to be able to
easily maintain the database. In this cleanup mechanism, one of the most
important processes are deleting log entries which are too old. But what does
too old means? It's different for different people. So I've decided to add a
parameter which defines this threshold or disables time based cleanup by
setting it to -1.

### max_command_length

Maximum command length is another one. Once I've listened to a talk about
hardening software interfaces, where the presenter said, it's usually a good
idea to limit your input size for each field. His example was to check what
happens in case of a user uploads an ISO as a profile picture. As you can guess
it can quickly fill up an object storage or even worse, a database. So then
I've decided to add a size check to every property of a public API what I
design or implement. It's better to drop requests with extreme properties early
instead of trying to filter out and delete them later. In this case, it can
occur for example that a customer accidently or intentionally sets the content
of an ISO as a command for this tool. To avoid storing these kind of inputs,
I've introduced the maximum command length parameter. If the given command is
longer than the value set by this parameter, then it should be simply truncated
to this limit. The default value is 4 kB. Anyway, if a command is longer than 4
kB then it's not readable. So it should not cause any trouble in real life use
cases.

## Selecting the language

The aim of the project to have a tool or script which creates the database
using the previously defined schema and manipulates some data using simple SQL
commands. It should be implemented as quickly as possible, because I really
need it. Because of this I had two choices, C and Python as they were the
languages at that time what I was most familiar with. So I weighed the pros and
cons of both.


|        | Pros                                                                           | Cons                                                                                                             |   |   |
|--------|--------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|---|---|
| C      | - Easy to transfer the compiled binary <br> - Runs similarly on all linux platforms | - Takes a bit more time to implement                                                                                        |   |   |
| Python | - Faster to implement                                                          | - Needs some dependencies to be installed <br> - Older Python versions on older operating systems may cause a problem |   |   |
|        |                                                                                |                                                                                                                  |   |   |

As I usually work on servers as well as on desktops and I have to use several
different linux distributions on them, so I chose C. It results in a single
binary, it runs on each distribution the same way, so it's perfect for me.
Maybe it takes a bit more time to implement, but this tool does not have so
much functionality anyway, so it can be done in a few hours or maybe days.

## Project name

The next step was to have a project name to be able to create the repository,
start the readme, etc. I went for the simple approach. As it is a tool for
storing history on a per directory basis, I choose dbhistory, which stands for
directory based history. It was maybe a wrong decision as it caused some
confusion because some person whom I've talked about it believed it to be a
tool for database history. But for now, it's good enough.

## User interface

As the requirement was to implement a simple command line, I only had to define
the command line usage, the help menu and the format of the history output.

### Command line usage

In this case, I chose the simple, industry standard approach. The tool should
be called with optional command line switches. So for example, the default
behavior is to list all history entries for the current working directory. For
that, no optional switch should be specified. For addition, there should be a
"-a" switch, followed by the command and so on. Based on this principle, the
following help menu was created:

```
Directory based command history.

Usage: dbhistory [OPTIONS] [COMMAND]
    -h Shows this help message
    -f Specify configuration file
    -c Cleans up the database
    -a Adds the COMMAND to the history db
    -s Search by applying given regex to pathes
```

"-h" will show this help menu. "-f" can be used to specify the path of the
configuration file. "-c" will run the cleanup routine. "-a", followed by a
command will add it to the history of the current working directory. "-s" will
allow to search based on a regular expression. If path matches with a regular
expression, then the history entries of that path will be returned. It can be
used for example to dump all the entries beloging to the current folder and the
folders below or to dump all entries in the database.

### History output

I was more or less happy with the output format of bash history, however I've
missed some useful information. It was the timestamp of the execution and the
folder in which it was executed. So my output would print a simple one-by-one
list of the history entries similarily to bash history with these two
additional fields, like the following snippet shows.

```
2020-02-11 21:17:49 /home/vargab95 vi
2020-02-11 23:48:20 /home/vargab95 ls -ltr
2020-02-11 23:48:22 /home/vargab95 suspend
```

## Build system

As I chose C as the project language, I've used CMake for the build system.
It's easy to use and I have experience with it so I use it in all my C projects
where it is possible.

# Implementation

I had the design for all major aspects, like the user interface, storage
solution, configuration, etc. The next step was to start the implementation.

## Layers

As this is a simple command line tool, I've decided to use a simple, three
layered architecture. The first is the interface logic, defined in the main.c
file. The second is the business logic, defined in a client.c module. The third
layer is the database layer. Furthermore, there are helper modules, for
logging, configuration, etc.

### First layer

This first layer has a main function and some helper functions. It handles the
user interface related tasks, like reading and processing command line
arguments, calling the helper for parsing the configuration, handles
configuration errors and starts the proper client command based on the
previously parsed command line arguments and configuration.

#### Main workflow

In this layer, I've decided to create and use a structure which contains all
the information which have been parsed from the command line arguments. One
function, called process_arguments fills this structure, then it is used to
parse the configuration and start the matching client call. I did this to make
sure that I could easily separate the command line argument handling and
command processing into different modules if I want later and this separation
improves testability as well.

#### Configuration handling

Global variables are considered to be bad and I usually agree with this
statement. On the other hand, in case of this tool I've use a global variable
to store the configuration. As always, it seemed to be a good idea during the
initial development phase. It's easy to implement, easy to use, you can reach
it from anywhere, etc and anyway in such a small project what harm can it
cause? However, after I've implemented and used it for a while, as always, it
turned out to be a pain rather then a great solution.

For example, as you could read above, max_command_length configuration
parameter was intended to limit the length of commands. However, as the
configuration could be reached from anywhere, I've "temporarily" used it to
limit maximal SQL command length executed by SQLite. After that, I've seen that
this parameter is used to limit the maximum command length, so I've totally
forgot to add the real command length validation feature. So using a global
variable indirectly caused the violation of a requirement in my opinion. This
kind of configuration should not have reached this SQL layer at all...
Furthermore, now the global configuration variable is used in just five
modules. So I could just pass the necessary configuration parameters into the
functions of these modules instead of creating a spaghetti by using a global
variable and make the code unclean and untestable. I learned my lesson again.
If I would like to write a testable and maintainable code, I would have to
avoid using global variables.

### Second layer

The second layer has some higher level functions mostly one per client
operation, like client_add_record. They implement the business logic of the
operations. For example, the mentioned client_add_record reads the current
working directory, connects to the database, creates a new history record and
closes the database connection.

### Third layer

The third layer provides functionality, like db_connect and db_add_record to
execute the SQL requests using SQLite. These commands are doing just basic SQL
operations. That's why I was astonished when I finished the development that
this data persistence layer gave about half of the complete code base.
Previously, I've used SQL in Python and NodeJS, where such persistence module
could be implemented by a few lines of code, by using an ORM or a query
builder. It was suprising how much code does it take to prepare SQL operation
strings, to parse results returned by the SQLite calls and to handle errors.

# Conclusion

This small tool turned to be a great help in my daily work. I use it several
times every day. So I would say the idea was a great one. Requirements were
also properly defined as I use all of them in my daily work, so I did not
invent something which would not be used anymore and I did not have to add a
lot more features to make it usable. Related to the architecture, in my opinion
it is also appropriate. It was easy to implement and use, I think, it's easily
extensible and handles errors properly.

The only thing what I would maybe change if I would start again is the language
choice. I love to code in C. It's a great language for low level and high
performance software. However, in this case it would have been much easier to
create just a python script which has modules like argparse for argument
parsing, configparser for parsing different type of configuration files, ORMs
like sqlalchemy and SQL query builders like pypika. In this case performance
and low level operations are not a concern, so I would not go with C again. It
would have taken much less time to finish this small tool in python or any
other scripting or higher level language.
