title: Real agility over scrum
author: Balázs János Varga
tags:
  - Agile
  - Scrum
categories:
  - Workflow
date: 2023-08-29 07:51:00
---

# Introduction

"Agile" – a term synonymous with adaptability in software development. But is
it all just talk? Many teams claim agility, yet stumble on the basics of the
Agile Manifesto. The core idea of valuing "individuals and interactions over
processes and tools" often gets overlooked. Let's delve into this divide.

# "agile" teams

I have been a developer in multiple teams. All of them claimed to be using
agile. However, when I asked, "Do you prioritize individuals and interactions
over processes and tools?" or any of the other principles outlined in the agile
manifesto, the response often began with "Well...".

In one of my recent interviews, the other party mentioned they were using scrum
but seemed unfamiliar with the manifesto.

The only common thread I could identify was that everyone claimed agility due
to their use of scrum. Wasn't the first point of the manifesto "Individuals
andinteractions over processes and tools"? Why do teams and companies proclaim
their agility while struggling with poor communication within and between
teams, and adhering to rigid processes that may not align with their needs or
those of their customers?

For example, in one case, a high priority issue could be solved in a day, but
it took two days to have all the necessary meetings and to convince the scrum
master to do it. Is it the flexible response that we want from agile?

# Non-scrum agile team

However, in one of my projects, I've met with something special. Of course it
was, stated during the interview, that agile processes are followed. However,
when I've started to experiment, the realisation came. They stated that they're
using scrum but it was far from the truth. They had a special kanban, scrum
variant which worked perfectly. We did not have processes and tools carved into
some stone tablets. The most important rule was to solve customer issues as the
first priority and beside those tasks having a user story which will improve
the product. That's more or less all. If it was kept by every team member we
could deliver a great customer experience in time. Furthermore, there were no
enforced rules, everybody could use their preferred IDE, way of working, etc.
The only important thing was to deliver a working software with the best
possible quality as soon as we can. With an emphasis on quality. Without daily
scrums, frequent retrospectives, plannings and demos, we could maintain a large
code base with 4 developers and a quite good response time for customer
requests. For example, fix for high priority errors could be delivered in the
same day. Of course, it may not work above a certain team size, but in this
case I was astonished how good it can be when the proper way of working is
found for the team.

# Conclusion

I don't say this approach is perfect for every team, but I'm sure of it that
more project should read up the agile manifesto multiple times, learn it by
heart and create a process which fits there needs and complies with the
manifesto rather than blindly following a predefined process.
