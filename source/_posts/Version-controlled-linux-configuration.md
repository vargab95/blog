title: Version controlled Linux configuration
author: Balázs János Varga
tags:
  - Command line
  - Linux
categories:
  - Workflow
date: 2023-04-13 21:44:00
---

# What is it about?

I've seen the idea of putting linux configuration files and dotfiles into a git
repository in [one of DT's videos](https://www.youtube.com/watch?v=tBoLDpTWVOM)
. It immediately caught my mind. I've struggled to remember to all the
different settings, all the modified configuration files, which were modified
in which machine, etc. How much easier it is to just pull and use all the new
settings on all systems. So based on the commands used in DT's video I've
created a similar [repository](https://gitlab.com/vargab95/devenv) for myself.

# What is it good for?

As it was already mentioned briefly in the first chapter, this project's main
purpose is to store important and non-default configuration files in a version
control system. As the configuration files will be stored in a version control
system, it brings all the benefits of these system. For example, I would be
able to
- roll back to any previous configuration on any of my systems if something
  goes wrong
- check the history of a certain configuration file and get an idea why a
  certain modification was made
- configure a newly installed system by simply cloning a repository
- distribute new configuration by simply pulling the changes

Beside that, I also use it for an additional purpose which was not mentioned in
the original video. As I usually use either debian or arch linux, I maintain a
list of installed packages and flatpaks in script files. With these script
files, I can easily install all the tools I need on a new system. I don't say
that it's always consistent and updated, but it gives a good starting point
each time I install a new system.

# What are the bad parts?

Every magic comes with a price. This approach also has some drawbacks as
everything else in life. Let's have a few words about those what I've
experienced since I'm using this approach.

## Similar != same

One of my major usecases, beside having the benefits of version controlled
files, was to create a consistent experience between my work environments. At
first, it sounds great, but then it turned out having similar systems does not
mean to have exactly the same systems. For example, in one of my projects, we
use a lot of environment variables to configure our system and we also have an
installation script which does some automatic changes to some of the dot files.
Due to this, I have to be careful at this system, because if I commit something
and I do not check it carefully, then I can commit some configuration which is
highly specific to that certain work environment. Of course, the majority of
these problems can be solved by adding something like an environment specific
bashrc, but then this method of configuration loses a lot of it's effectiveness
because only just a part of the configuration will be stored, so it won't be
easily reproducible. Furthermore, it won't solve the problem of magical scripts
which modifies existing dot files.

## Status of new files

As the root of this repository is placed in the home folder of the user, it
does not make sense to list all the files on a git status command. As it would
print out all the non tracked files. This can be solved by configuring git to
not show untracked files, but it brings some negative affects. Due to this
problem, the user won't see when a newly introduced configuration file is not
tracked. I had this experience a few times that I cloned the repository on a
new machine and some of my tools behaved wierdly. Then I realized that I forgot
to upload their configuration file.

## Different defaults

Different operating systems comes with different default configuration files.
This is not a problem until you install the system, modify the configuration
files according to your needs and use your shiny new system. However, if you
store your configuration files in a version control system, then you will have
the same configuration files through each system. For example bashrc can have
some platform specific configuration. Furthermore, different distributions can
have different version of application in their repository. In this case, there
can be a conflict between the configuration of a package downloaded from the
oldstable debian repository and the arch linux repository. There can be major
version differences between these two.

# How do I use it?

As it was recommended in the video, I've created an alias to this command.
Using this alias, it can be used as same as a normal git repository.

## Modification

When I do a modification to one of my configuration files or if I edit a new
one, then I add-commit-push them.

## Update

Regarding to the updates, there is no certain rule for that. As changes are not
so frequent, I just run a "dotconf pull" once in a while when I start one of my
linux installations and then I get all the new changes.

## New installation

When I install a new linux system, then one of my first steps is to go to
gitlab, clone my devenv repository and to copy and paste the commands from the
repository's readme.

# Conclusion

As you can see, everything has it's good and bad aspects. It's the same with
such a simple solution like this. However, I use this approach for more than a
year at the time of writing and it helps me a lot even with these minor
drawbacks. So I would recommend this approach for everyone who uses a linux
distribution in his or her work or daily life, even if they use only one
installation.
