title: Performance vs clean code
author: Balázs János Varga
tags:
  - Design
categories:
  - Software design
date: 2023-07-31 22:12:00
---

As software developers, we find ourselves grappling with an age-old debate:
Should we prioritize clean code principles or focus on performance
optimization? Each approach garners fervent supporters, often leading to
passionate debates and highly opinionated videos (like
[this](https://www.youtube.com/watch?v=tD5NrevFtbU), as developers engage in
spirited discussions over the merits of clean code versus performance
optimization within the development community. However, I firmly believe that
the answer lies in finding a harmonious balance between the two, as it
ultimately depends on the specific requirements and context of each project.

# Embracing the Duality

In my journey as a software developer, I have had the privilege of working in
diverse domains, ranging from database development in C to creating web, data
processing, desktop, and mobile applications using Python, Node.js, and Dart.
This unique perspective has enabled me to appreciate the merits of both clean
code and performance optimization approaches.

## The Need for Speed: Prioritizing Performance

In some scenarios, performance is of paramount importance. For instance, when
dealing with resource-constrained environments or CPU-intensive tasks,
optimizing for speed becomes a must. In these cases, low-level languages like C
and assembly, combined with performance tricks, can significantly enhance a
system's efficiency.

Example: When I had to develop a high-performance protocol translator, I turned
to C. Its efficiency in memory manipulation using pointers allowed me to
achieve the desired execution speed while maintaining a development speed
suitable for the project's needs.

## The Elegance of Clean Code

On the other hand, we encounter situations where maintainability, readability,
and modular design are the primary concerns. Clean code principles come into
play when the focus is on writing code that is easy to understand, refactor,
and extend. High-level languages like Python, with their convenient
abstractions, become the tool of choice in such scenarios.

*Example*: While building a trading system, I opted for Python. Given that the
most time-consuming aspect was waiting for external exchanges, the readability
and development speed provided by Python outweighed the need for ultimate
performance.

## Finding the Sweet Spot: A Balanced Approach

While it's tempting to pick a side and adhere strictly to one philosophy, the
reality is more nuanced. The optimal choice depends on a careful assessment of
the project's unique needs.

### The Scale of Trade-offs

The choice between clean code and performance optimization exists on a scale,
with multiple points in between. For instance:

- For moderately performance-critical applications, languages like Go or Rust
  offer a balance between developer convenience and performance.
- When projects demand the convenience of a virtual machine and moderate
  performance, languages like Java or C# can be viable choices.
- For quick scripting and development, scripting languages like Python excel in
  providing rapid prototyping and implementation.

### The Role of Premature Optimization

Premature optimization can lead to wasted efforts and negligible gains. It's
often better to focus on building a functional system rapidly, validating it in
the market, and optimizing based on real-world usage if necessary.

*Example*: Imagine a scenario where the preparation of a complex database
request takes 1 ms, while executing the query itself takes 100 ms. By
optimizing the preparation process to 0.1 ms using low-level performance
tricks, you would reduce the overall execution time from 101 ms to 100.1 ms.
However, such marginal gains might not justify the extra development overhead.
In such cases, using a high-level language to implement the system quickly can
be a more pragmatic approach.

# Conclusion

As software developers, we are often faced with the dilemma of choosing between
clean code and performance optimization. However, the key to success lies in
finding the right balance. Instead of dogmatically adhering to one approach, we
should pragmatically assess the specific needs of each project and adapt
accordingly.

By embracing the duality of software development, we can leverage low-level
languages and performance tricks when efficiency is critical. Simultaneously,
we can employ high-level languages to accelerate development speed.

Remember, software development is not a one-size-fits-all endeavor. The true
measure of success lies in crafting elegant, efficient, and functional
solutions that fulfill the unique requirements of each project. Strive for a
balanced approach, and you'll become a proficient software developer capable of
tackling diverse challenges with finesse.
