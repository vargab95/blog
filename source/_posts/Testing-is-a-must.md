title: Testing is a must
author: Balázs János Varga
tags:
  - Testing
  - Software development
categories:
  - Testing
  - Workflow
date: 2023-06-20 14:36:00
---

# Testing is a must

As software developers, our primary goal is to deliver high-quality code that
meets the expectations of our users and stakeholders. We continuously encounter
challenges in identifying and fixing bugs effectively. In this article, I will
share my personal journey and evolving perspective as a developer, shedding
light on the essential role of testing in new feature development.

## Testing in fixing bugs

When encountering a bug, my preferred approach is to utilize Test-Driven
Development principles. The first step is to reproduce the issue.
Reproducibility is crucial because until we can consistently replicate the
problem, we cannot be certain that we have truly identified its root cause. As
renowned physicist Richard Feynman once said, "What I cannot create, I do not
understand." By reproducing the bug, we gain a deeper understanding of its
nature.

Once I have successfully reproduced the bug, my next step is to write a
regression test case. This approach offers several benefits. Firstly, the
regression test case ensures that the specific problem will be automatically
tested in the future, safeguarding against its recurrence. Additionally,
running the test case with official builds and on official platforms provides a
more accurate assessment of the bug's resolution. By working on the bug until
it is fixed across all relevant platforms and versions, we can deliver a robust
solution.

## Testing in feature development

While TDD proves invaluable in bug fixing, some developers question its
relevance in feature development. It is reasonable to wonder why we should
write tests first when the new feature may not be clearly defined or undergoes
frequent changes during the development process. In such cases, the tendency is
to implement the feature first, seek stakeholder agreement, and then write the
tests. However, as we know, some developers do not like to write tests or claim
they do not have time for it, especially when testing is the last task. I, too,
had to learn, at my own expense, that TDD can also be highly advantageous in
feature development.

Allow me to share a personal experience. A few months ago, I was tasked with
implementing a new feature in one of my projects. After completing the
implementation, I performed thorough manual testing and subsequently released
it to users. Initially, I believed that delaying test writing until after
release or modification or just doing manual tests was not problematic. I was
confident in my ability to adequately test the software during development, and
I did indeed test it - for the first time.

However, a few weeks later, a simple change request emerged. It appeared
straightforward, requiring just a few lines of code changes and a minor
refactor. Unfortunately, I made the mistake of assuming that this simple change
was too trivial to fail, and therefore decided not to retest the previously
implemented feature manually. With no automated tests to catch any potential
issues and no additional manual testing from others, I released the updated
version. The repercussions were immediate when customers encountered the flaw
within minutes. I was forced to swiftly revert the changes, and the entire
situation was both embarrassing and avoidable. This incident emphasized the
importance of writing at least a simple, happy path test case during the
initial implementation of a feature.

## Changing Perspectives and Lessons Learned

Reflecting on my experience, I find my perspective on testing evolving. It is
becoming apparent that adopting a test-first approach, or at the very least,
not considering a feature done until a regression test case exists for the
happy path, can significantly reduce the likelihood of future problems.
Unfortunately, developers sometimes overlook the importance of testing,
particularly when it is not their initial task. Consequently, there is a risk
that tests may not be written once the implementation is complete, which can
result in issues arising further down the line. I learned this lesson the hard
way, realizing the importance of thorough testing throughout the development
process.

In conclusion, my personal journey has taught me the invaluable lesson that
testing is a non-negotiable aspect of new feature development. It is not a
superfluous burden or a time-consuming activity but rather a vital investment
in the quality and success of our software. Let us embrace testing as an
integral part of our development process and deliver exceptional products that
exceed expectations.
