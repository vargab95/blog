title: Per directory pinnings
author: Balázs János Varga
tags:
  - C
  - Command line
  - Linux
categories:
  - Software design
date: 2023-03-24 13:11:45
---

This is a continuation of my [previous
post](/2023/03/24/Per-directory-history/) about per directory history.

After I finished the implementation of the first stable version of the
dbhistory tool, I decided to ask other people's opinion about it. Maybe they
have some ideas which could help to improve this tool or my workflows or maybe
they will find it useful and start to use it.

To achieve it, I created a [post](https://www.reddit.com/r/commandline/comments/wctftx/dbhistory_per_directory_history_for_bash)
in r/commandline. I was really glad, because some people has answered my
question and one of them had a great idea. He said that it could be really
useful to pin some history entries. I've not realised until then how good this
idea was. One of the major use cases of this tool was to be able to quickly
find the most important commands for a project or task. Of course, it can be
much easier if you would not have to go through all the command executed in
this folder but you could just simply show, copy and paste it. It was such a
good idea in my opinion that I started to work on it immediately.

As this change did not require any complicated or interesting implementation,
this post will be more about the idea, the design decisions and details and a
retrospective about these decisions rather then going into why have I put an if
statement there.

# Requirements

As a first step, I had to define the requirements of the new feature. It's
great to have a new idea, but in my opinion, it's not the best way to start to
implement something without having at least the high level requirements
defined.

To be able to use such a feature, at least the following new functionality must
be implemented:
- Be able to pin a command to a folder
- Be able to show pinned commands of the current folder
- Be able to show pinned commands in a search result
- Be able to unpin commands

Furthermore, I had the idea to not break the old behavior. So the tool should
have a configuration option to disable this new feature if the user does not
want to use it. To achieve this, a new configuration option was introduced,
named use_pinnings.

# Database

As I described in my [previous post](/2023/03/24/Per-directory-history/),
SQLite was chosen as a storage engine. So to be able to store pinnings, I had
to update my database schema. To achieve it, I extended the already existing
schema according to the following diagram.

<div class="plantuml-container">
```plantuml
@startuml
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity path_map {
  *id: number <<generated>>
  --
  *path: text
}

entity history {
  *id: number <<generated>>
  --
  *path_id: number <<FK>>
  *command: text
  *timestamp: datetime
}

entity pinnings  {
  *id: number <<generated>>
  --
  *path_id: number <<FK>>
  *command: text
}

path_map ||..o{ history
path_map ||..o{ pinnings
@enduml
```
</div>

In the code, a new entry was added to the initialization script which creates
the table if it does not exists. It provides a simple way to be able to create
new databases and update already existing ones as well.

# User interface

## Command line arguments

The user interface also had to be extended. It got three new commands.

```
    -p Pin a command to the current folder
    -u Unpin a command by ID
    -o Show only the history without pinnings
```

-o was for backward compatible print, so if the user wanted to only have the
history of the folder or the search result, then he/she could specify -o and
just get the old style output.

-p was for pinning a new command. It takes the next argument and pins it to the
current directory.

-u takes the id of a pinning and removes it from the table.

## Printout

As there are two kind of different display commands, two separate outputs was
necessary as well.

### List current directory

The first is when the commands of the current directory is listed. It should
look like. It may look strange that both output will have an ID field. It's not
the nicest way, but it's required to be able to unpin commands. Of course, it
could have been implemented to use the path and the command to delete a
pinning, but in my opinion, using an identifier is much more convenient.

```
Pinnings:
 ID  Command
  9  echo
 10  clear
```

### List search result

The second is when the commands of search result are listed. As it can contain
pinnings for different pathes, the simple ID COMMAND approach is not good
enough. It should be extended by the path, so it should look like.

```
Pinnings (ID Path Command):
  7 /home/vargab95/ echo
  8 /home/vargab95/ clear
```

# Configuration

For the backward compatibilty option, a new configuration option was added.
It's called use_pinnings. By default, it's set to 1, which means the feature is
enabled by default. However if a user do not want to use it, he or she can
disable it.

# Implementation

I won't go into the details of the implemetation as there were not so much
about it. It was done by simply adding a few new functions to the client and
database layers which is really similar to the already existing commands for
adding, searching and listing history entries. If somebody is interested in the
implementation itself, they can check my commit [here](https://gitlab.com/vargab95/dbhistory/-/commit/3c654b93275b50800e92a166bb23c2acb2b0c0a3).

# Retrospection

## Configuration for backward compatibility

Later, it turned out that configurable backward compatibility was a bad idea.
It has increased the code complexity and anyway, if somebody have not added a
pinning, then the behavior was the same... Maybe in the future I'll completely
remove this option. However, the take away is that, if a new feature won't make
any incompatibility issues, then do not make it configurable. If it would have
been a tool which is used by thousands of people, then I would not be able to
revert this change in the future, because it would be a breaking change after a
release which included this option. So my opinion has changed a bit. If it's
not necessary, do not introduce feature switches. Of course, if it would have
been a completely different way to for example add commands, then it's a
different situation, but for just an additional feature, it should not be
added.

## Database choice

However, choosing RDBMS as a storage option turned to be a great choice. If I
would have chosen for example a file based approach it would have been much
more difficult to add this new feature.
