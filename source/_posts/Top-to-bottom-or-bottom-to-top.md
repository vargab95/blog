title: Top to bottom or bottom to top: Approaches to software design 
author: Balázs János Varga
tags:
  - Design
categories: 
  - Software design
  - Workflow
date: 2023-08-07 22:01:20
---

When I teach programming, several students ask which one is better, to design
and implement top to bottom or bottom to top. So, to start with the details or
start from more or less the main function. As this question appears again and
again, I decide to create a post about this topic.

In my opinion, both top to bottom and bottom to top design and implementation
are acceptable approaches. However, it depends more on the problem domain. I
know some developers who have a clear preference over one or the other, but I
think it's not a one-time choice that can be used for every problem.

# Bottom to top

I usually use the bottom-top approach when a project has a low-level
requirement that is critical. For example, if there is an automatic trading
system, then the most important part is the trading itself. If there is no easy
way to access exchanges or there are some problems in the interface design of
these low-level exchange access boundaries, then it can have a nice UI, access
management, or calculation interface, but nothing will work. So in these cases,
at first, the low-level interfaces must be defined and tested thoroughly. After
the foundation of the project works, then the upper layers can be built and
tested on top of it in a step-by-step manner.

# Top to bottom

On the other hand, when there is a well-defined set of high-level functions,
but there are no critical low-level details, then I usually use the top to
bottom approach. For example, in the case of simple, CRUD REST interfaces, it's
usually better to create an OpenAPI specification at first. If there is another
developer who'll use the API, then he or she should be involved as well. After
it's defined, some test cases can be written. If they are done, then the
implementation of the high-level functions can be started, and it can be
checked whether the underlying service was called at all. Following this
principle, developers can slowly add all the components and finish with the
lowest level of details, like implementing the storage boundary interfaces for
database access.

In the case of such a project, it's clearly a top-bottom approach for me. It
has a well-defined set of functional requirements, and there are no critical
details in the low-level components. For example, simple files can easily be
used to store this information if it turns out later that would be the best
storage approach. It'll only be required to change the lower database access
layer, which can be changed easily.

# Selecting between the two

In my opinion, the best point to start is the most critical point. Anyway, if
the most critical point fails, then the rest is kind of meaningless. That's why
I would say that if the project has clear external requirements, then the
top to bottom approach is the clear winner. If there are certain low-level
requirements, like a hardware interface in an embedded system, an external
dependency in a server or desktop application, or some peripheral or API in a
mobile application, then start with the low level. Create a proper abstraction
above it and start to build upon that step by step. If the business logic or a
part of it is the most critical part, then build it first, write tests for it,
and build the rest around it. Always choose a technique for the task and not in
the opposite direction.

# Summary

To summarize the two approaches, as usual, it depends. My opinion is to do the
most critical part first and then build the rest around it.
